package pl.edu.pwsztar.domain.mapper;

import org.springframework.stereotype.Component;
import pl.edu.pwsztar.domain.dto.MovieCounterDto;

@Component
public class CounterMapper {
    public MovieCounterDto mapToDto(long counter) {
        MovieCounterDto movieCounterDto = new MovieCounterDto();

        movieCounterDto.setCounter(counter);

        return movieCounterDto;
    }
}
