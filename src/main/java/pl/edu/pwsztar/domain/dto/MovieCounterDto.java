package pl.edu.pwsztar.domain.dto;

public class MovieCounterDto {
    private long counter;

    public MovieCounterDto() {
    }

    public MovieCounterDto(long counter) {
        this.counter = counter;
    }

    public long getCounter() {
        return counter;
    }

    public void setCounter(long counter) {
        this.counter = counter;
    }


    @Override
    public String toString() {
        return "DetailsMovieDto{" +
                "counter='" + counter + '\'' +
                '}';
    }
}
